﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eStoreModels
{
    /// <summary>
    /// BranchModel
    ///     Author:     Jon Decher
    ///     Date:       Created: Aug 12, 2013
    ///     Inherits:   Standard config data
    ///     Purpose:    Data Layer Class to interface with Database, will serve data to the 
    ///                 BranchViewModel class
    ///     Revisions:  none
    /// </summary>
    public class BranchModel : eStoreModelConfig
    {
        /// <summary>
        /// Get Closest Branches
        /// </summary>
        /// <param name="dictionaryAddresses">latitude and longitude coordinates determined by google</param>
        /// <returns>List of details of 3 closet branches</returns>
        public List<ClosestBranches> GetClosetBranches(Dictionary<string, object> dictionaryAddresses)
        {
            eStoreDBEntities dbContext;
            List<ClosestBranches> branchDetails = new List<ClosestBranches>();

            try
            {
                dbContext = new eStoreDBEntities();
                float? latitude = Convert.ToSingle(dictionaryAddresses["lat"]);
                float? longitude = Convert.ToSingle(dictionaryAddresses["long"]);
                // executes stored proc via EF function
                branchDetails = dbContext.GetThreeClosestBranches(latitude, longitude).ToList();
            }
            catch (Exception e)
            {
                ErrorRoutine(e, "OrderModel", "GetClosestBranches");
            }
            return branchDetails;
        }
    }
}
