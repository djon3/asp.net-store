﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using eStoreViewModels;
using WebMatrix.WebData;

namespace eStoreWebsite.Controllers
{
    /// <summary>
    /// CartController
    ///     Author:     Jon Decher
    ///     Date:       August 16, 2013         
    ///     Purpose:    Controller responsible for selecting a view for presenting information to the user.
    ///     Revisions:  Added a generate order method thats will add the order to the database.
    /// </summary>
    public class CartController : Controller
    {
        //
        // GET: /Cart/
        /// <summary>
        /// Shows the view of the Cart
        /// </summary>
        /// <returns>View</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Generates an order and adds the entire order to the database
        /// </summary>
        /// <returns></returns>
        public ActionResult genOrder()
        {
            OrderViewModel myOrder = new OrderViewModel();
            CustomerViewModel cust = new CustomerViewModel();
            try
            {

                myOrder.AddOrder((CartItemViewModel[])Session["Cart"],
                                  Convert.ToInt32(Session["CustomerID"]),
                                  (double)Session["OrderAmount"]);
                
               
                List<CustomerViewModel> custList = cust.GetAllCustomers();
                CustomerViewModel cvm = new CustomerViewModel();

                foreach (CustomerViewModel c in custList)
                {                    

                    if (c.CustomerID == Convert.ToInt32(Session["CustomerID"]))
                    {
                        if (myOrder.OrderID > 0) //order added
                        {
                            ViewBag.Message = "Order " + myOrder.OrderID + " created!";

                            MailMessage msg = new MailMessage();
                            msg.Subject = "New Order";
                            msg.From = new MailAddress("registrar@pintinmyglass.com", "Pint in My Glass Registrar");
                            msg.To.Add(new MailAddress(c.Email));
                            msg.Body = "Congratulations " + c.FirstName +
                                       ", you have successfully ordered! Your new " +
                                       "order # is " + myOrder.OrderID + ".";
                            using (SmtpClient mailClient = new SmtpClient())
                            {
                                mailClient.Send(msg);
                            }

                            if (myOrder.BackOrderFlag > 0)
                                ViewBag.Message += "Some goods were backordered!";

                        }
                        else
                            ViewBag.Message = myOrder.Message + ", try again later!";
                    }
                }
               
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Order was not created, try again later! - " + ex.Message;
            }

            return PartialView("PopupMessage");
        }

    }
}
