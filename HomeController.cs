﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData; 
using System.Web.Security;
using eStoreWebsite.Filters;
using eStoreViewModels;
using System.Net.Mail;

namespace eStoreWebsite.Controllers
{
    /// <summary>
    /// HomeController
    ///     Author:     Jon Decher
    ///     Date:       August 16, 2013         
    ///     Purpose:    Controller responsible for selecting a view for presenting information to the user.
    ///     Revisions:    Added allow anonymous so only logged in users can use use the site.
    /// </summary>
    [InitializeSimpleMembership]
    public class HomeController : Controller
    {
        /// <summary>
        /// Checks if a user is logged in or not. 
        /// Shows user that no one is logged in.
        /// Returns the view for the homepage.
        /// </summary>
        /// <returns>View</returns>
        [AllowAnonymous]
        public ActionResult Index()
        {
            if(Session["Message"] == null)
            {
                Session["Message"] = "Please login first!";
            }

            if (Session["LoginStatus"] == null)
            {
                Session["LoginStatus"] = "not logged in";
            }
            return View();
        }

        /// <summary>
        /// Registers a user. Creates a user and account.
        /// </summary>
        /// <param name="cust"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Register(eStoreViewModels.CustomerViewModel cust)
        {
            try
            {
                ViewBag.Message = "";
                WebSecurity.CreateUserAndAccount(cust.UserName, cust.Password);
                cust.Register();
                if (cust.CustomerID > 0)
                {
                    ViewBag.Message = cust.Message;
                    MailMessage msg = new MailMessage();
                    msg.Subject = "New Registration";
                    msg.From = new MailAddress("registrar@pintinmyglass.com", "Pint in My Glass Registrar");
                    msg.To.Add(new MailAddress(cust.Email));
                    msg.Body = "Congratulations " + cust.FirstName +
                               ", you have been registered at Pint in My Glass!" +
                               " your new username is " + cust.UserName;
                    using (SmtpClient mailClient = new SmtpClient())
                    {
                        mailClient.Send(msg);
                    }
                }
                else
                {
                    ViewBag.Message = "Problem Registering, try again later";
                    ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(cust.UserName);
                    ((SimpleMembershipProvider)Membership.Provider).DeleteUser(cust.UserName, true);

                }
                ViewBag.Message = cust.Message;
                return PartialView("PopupMessage");
            }
            catch (MembershipCreateUserException e)
            {
                ViewBag.Message = ErrorCodeToString(e.StatusCode);
                return PartialView("PopupMessage");
            }
        }

        /// <summary>
        /// Generates an error if input is invalid or duplicate
        /// </summary>
        /// <param name="createStatus"></param>
        /// <returns></returns>
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        /// <summary>
        /// Logs a user in and shows which user is logged in.
        /// </summary>
        /// <param name="cust"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Login(eStoreViewModels.CustomerViewModel cust)
        {
            try
            {
                if (WebSecurity.Login(cust.UserName, cust.Password))
                {
                    cust.GetCurrentProfile();
                    Session["CustomerID"] = cust.CustomerID;
                    Session["Message"] = "Welcome " + cust.UserName;
                    Session["LoginStatus"] = "Logged in as " + cust.UserName;
                    return Json(new { url = Url.Action("") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ViewBag.Message = "login failed - try again";
                    return PartialView("PopupMessage");
                }                
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Severe Error on Login ";
                String exMsg = ex.Message;
                return PartialView("PopupMessage");
            }
        }

        /// <summary>
        /// Logs the current user out
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            WebSecurity.Logout();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }    
}
