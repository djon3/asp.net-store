﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
 
namespace eStoreModels
{
    /// <summary>
    /// OrderModel
    ///     Author:     Jon Decher
    ///     Date:       August 16, 2013
    ///     Inherits:   Standard config
    ///     Purpose:    Data layer class to interface with database,
    ///                 will serve and receive data from 
    ///                 OrderViewModel class. Will add an order to the 
    ///                 database
    ///     Revisions:  Added methods that will retreive information from the database 
    ///                 about customers and orders and send them up to the view model layer.
    /// </summary>
    public class OrderModel : eStoreModelConfig
    {
        /// <summary>
        /// Adds order informaiton to the database according to the customer
        /// that is currently logged in.
        /// </summary>
        /// <param name="bytOrder"></param>
        /// <returns>a serialized dictionary</returns>
        public byte[] AddOrder(byte[] bytOrder)
        {
            Dictionary<string, Object> dictionaryOrder = (Dictionary<string, Object>)Deserializer(bytOrder);
            Dictionary<string, Object> dictionaryReturnValues = new Dictionary<string, Object>();
            //deserialize into local variables
            int[] qty = (int[])dictionaryOrder["qty"];
            string[] prodcd = (string[])dictionaryOrder["prodcd"];
            Decimal[] sellPrice = (Decimal[])dictionaryOrder["msrp"];
            int cid = Convert.ToInt32(dictionaryOrder["cid"]);

            bool boFlg = false;
            Decimal oTotal = Convert.ToDecimal(dictionaryOrder["amt"]);
                     
            eStoreDBEntities dbContext;

            using (TransactionScope transaction = new TransactionScope())
            {

                try
                {
                    dbContext = new eStoreDBEntities();
                    Order myOrder = new Order();

                    //back to the db to get the right Customer based on session customer id
                    var selectedCusts = dbContext.Customers.Where(c => c.CustomerID == cid);
                    myOrder.Customer = selectedCusts.First();

                    myOrder.OrderDate = DateTime.Now;
                    myOrder.OrderAmount = oTotal;
                    dbContext.Orders.Add(myOrder);

                    for(int idx = 0; idx < qty.Length; idx++)
                    {
                        if(qty[idx] > 0)
                        {
                            OrderLineitem item = new OrderLineitem();
                            string pcd = prodcd[idx];
                            var selectedProds = dbContext.Products.Where(p => p.ProductID == pcd);
                            item.Product = selectedProds.First();//got product for item

                            if(item.Product.QtyOnHand >qty[idx]) // enough stock
                            {
                                item.Product.QtyOnHand = item.Product.QtyOnHand - qty[idx];
                                item.QtySold = qty[idx];
                                item.QtyOrdered = qty[idx];
                                item.QtyBackOrdered = 0;
                                item.SellingPrice = sellPrice[idx];
                            }
                            else //not enough stock
                            {
                                item.QtyBackOrdered = qty[idx] - item.Product.QtyOnHand;
                                item.Product.QtyOnBackOrder = item.Product.QtyOnBackOrder + (qty[idx] - item.Product.QtyOnHand);
                                item.Product.QtyOnHand = 0;
                                item.QtyOrdered = qty[idx];
                                item.QtySold = item.QtyOrdered - item.QtyBackOrdered;
                                item.SellingPrice = sellPrice[idx];
                                boFlg = true; // something backordered
                            }
                            myOrder.OrderLineitems.Add(item);
                        }
                    }
                    
                    dbContext.SaveChanges();
                  //  throw new Exception("Rollback");

                    transaction.Complete();

                    dictionaryReturnValues.Add("orderid", myOrder.OrderID);
                    dictionaryReturnValues.Add("boflag", boFlg);
                    dictionaryReturnValues.Add("message", "");
                }
                catch (Exception ex) {
                    ErrorRoutine(ex, "OrderModel", "AddOrder");
                    dictionaryReturnValues.Add("message", "Problem with Order");
                }
                return Serializer(dictionaryReturnValues);
            }
        }

        /// <summary>
        /// Retrieves all order information of all customers
        /// </summary>
        /// <returns>a list of all order information</returns>
        public List<Order> GetAllForCust()
        {
            eStoreDBEntities dbContext;
            List<Order> allOrders = null;
            try
            {
                dbContext = new eStoreDBEntities();
                allOrders = dbContext.Orders.ToList();
            }
            catch (Exception ex)
            {
                ErrorRoutine(ex, "OrderData", "GetAllForCust");
            }
            return allOrders;
        }

        /// <summary>
        /// Get all details for all orders for a customer.
        /// </summary>
        /// <param name="custid"></param>
        /// <returns>a list of all order details</returns>
        public List<OrderDetailModel> GetAllDetailsForAllOrders(int custid)
        {
            List<OrderDetailModel> allDetails = new List<OrderDetailModel>();

            try
            {
                eStoreDBEntities db = new eStoreDBEntities();

                var results = from o in db.Orders
                              join l in db.OrderLineitems on o.OrderID equals l.OrderID
                              join p in db.Products on l.ProductID equals p.ProductID
                              where (o.CustomerID == custid)
                              select new OrderDetailModel
                              {
                                  CustomerID = o.CustomerID,
                                  OrderAmount = o.OrderAmount,
                                  OrderID = o.OrderID,
                                  ProductName = p.ProductName,
                                  SellingPrice = l.SellingPrice,
                                  QtySold = l.QtySold,
                                  QtyOrdered = l.QtyOrdered,
                                  QtyBackOrdered = l.QtyBackOrdered,
                                  OrderDate = o.OrderDate
                              };

                allDetails = results.ToList<OrderDetailModel>();

            }
            catch (Exception e)
            {
                ErrorRoutine(e, "OrderData", "GetAllDetailsForAllOrders");
            }
            return allDetails;
        }


    }
}
