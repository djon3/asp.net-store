﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eStoreModels;

namespace eStoreViewModels
{
    /// <summary>
    /// OrderViewModel
    ///     Author:     Jon Decher
    ///     Date:       August 16, 2013         
    ///     Purpose:    View model containing and AddOrder method. 
    ///                 Allows order communication between the model and the user.
    ///     Revisions:  Added methods to reteive information about the customer and the order 
    ///                 from the model layer and send it up to the presentation layer.
    /// </summary>
    public class OrderViewModel : eStoreViewModelConfig
    {
        /// <summary>
        /// Properties
        /// </summary>
        public int OrderID ;
        public int BackOrderFlag ;
        public string Message ;
        public int CustomerID;
        public DateTime OrderDate;

        /// <summary>
        /// Packages order information up and sends it to the model layer
        /// </summary>
        /// <param name="items"></param>
        /// <param name="cid"></param>
        /// <param name="amt"></param>
        public void AddOrder(CartItemViewModel[] items, int cid, double amt)
        {
            Dictionary<string, Object> dictionaryReturnValues = new Dictionary<string, object>();
            Dictionary<string, Object> dictionaryOrder = new Dictionary<string, object>();

            try
            {
                Message = "";
                BackOrderFlag = 0;
                OrderID = -1;
                OrderModel myData = new OrderModel();
                int idx = 0;
                string[] prodcds = new string[items.Length];
                int[] qty = new int[items.Length];
                Decimal[] sellPrice = new Decimal[items.Length];

                foreach (CartItemViewModel item in items)
                {
                    prodcds[idx] = item.ProdCd;
                    sellPrice[idx] = item.Msrp;
                    qty[idx++] = item.Qty;
                }

                dictionaryOrder["prodcd"] = prodcds;
                dictionaryOrder["qty"] = qty;
                dictionaryOrder["msrp"] = sellPrice;
                dictionaryOrder["cid"] = cid;
                dictionaryOrder["amt"] = amt;
                dictionaryReturnValues = (Dictionary<string, Object>)Deserializer(myData.AddOrder(Serializer(dictionaryOrder)));
                OrderID = Convert.ToInt32(dictionaryReturnValues["orderid"]);
                BackOrderFlag = Convert.ToInt32(dictionaryReturnValues["boflag"]);
                Message = Convert.ToString(dictionaryReturnValues["message"]);
            }
            catch (Exception ex) {
                ErrorRoutine(ex, "OrderViewModel", "AddOrder");
                dictionaryReturnValues.Add("message", "Problem with Order");
            }

        }

        /// <summary>
        /// Gets all information for the current customer from the model layer 
        /// then sends the needed information up to the presentation layer.
        /// </summary>
        /// <returns>A list of orders to the presentation layer</returns>
        public List<OrderViewModel> GetAllForCust()
        {
            List<OrderViewModel> ord = new List<OrderViewModel>();
            try
            {
                OrderModel data = new OrderModel();
                List<Order> dataOrders = data.GetAllForCust();


                foreach (Order o in dataOrders)
                {
                    OrderViewModel ovm = new OrderViewModel();
                    ovm.CustomerID = o.CustomerID;
                    ovm.OrderDate = o.OrderDate;
                    ovm.OrderID = o.OrderID;                    

                    ord.Add(ovm); //add to list                    
                }
            }
            catch (Exception ex)
            {
                ErrorRoutine(ex, "OrderViewModel", "GetAllForCust");
            }
            return ord;
        }

        /// <summary>
        /// Gets all information for the current order from the model layer 
        /// then sends the needed information up to the presentation layer.
        /// </summary>
        /// <returns>a list of details of order up to the presentation layer</returns>
        public List<OrderDetailViewModel> GetAllDetailsForAllOrders()
        {
            List<OrderDetailViewModel> viewModelDetails = new List<OrderDetailViewModel>();

            try
            {
                OrderModel myData = new OrderModel();
                List<OrderDetailModel> modelDetails = myData.GetAllDetailsForAllOrders(CustomerID);

                viewModelDetails = modelDetails.ConvertAll(new Converter<OrderDetailModel,
                                                               OrderDetailViewModel>(ModelToViewModel));

            }
            catch (Exception ex)
            {
                ErrorRoutine(ex, "OrderViewModel", "GetAllDetailsForAllOrders");
            }
            return viewModelDetails;
        }

        /// <summary>
        /// Gets model information and puts it into the coresponding model information 
        /// </summary>
        /// <param name="o"></param>
        /// <returns>v</returns>
        private OrderDetailViewModel ModelToViewModel(OrderDetailModel o)
        {
            OrderDetailViewModel v = new OrderDetailViewModel();
            v.CustomerID = o.CustomerID;
            v.OrderAmount = o.OrderAmount;
            v.OrderID = o.OrderID;
            v.ProductName = o.ProductName;
            v.QtyBackOrdered = o.QtyBackOrdered;
            v.QtyOrdered = o.QtyOrdered;
            v.QtySold = o.QtySold;
            v.SellingPrice = o.SellingPrice;
            v.OrderDate = o.OrderDate;
            return v;
        }


    }
}
